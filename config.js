'use strict'// 严格模式

var path = require('path')
var util = require('./libs/util')
var wechat_file = path.join(__dirname, './config/wechat_file.txt')
var wechat_ticket_file = path.join(__dirname, './config/wechat_ticket_file.txt')

var config = {
	// 参数声明
	weChat:{
		appID: 'wx920394da26edc0c4',
		appSecret: '18e73c8254349ae44bc27030aa5b53ca',
		token: 'songqiang880202',
		// 声明获取、更新token的方法
		getAccessToken: function() {
			// 调用util读取文件的方法
			return util.readFileAsync(wechat_file)
		},
		saveAccessToken: function(data){
			data = JSON.stringify(data)
			// 调用util写文件的方法
			return util.writeFileAsync(wechat_file,data)
		},
		getTicket: function() {
			// 调用util读取文件的方法
			return util.readFileAsync(wechat_ticket_file)
		},
		saveTicket: function(data){
			data = JSON.stringify(data)
			// 调用util写文件的方法
			return util.writeFileAsync(wechat_ticket_file,data)
		}
	}
}

module.exports = config