'use strict'// 严格模式

var Koa = require('koa')
var path = require('path')

var convert = require('koa-convert');
var wechat = require('./wechat/g')
var util = require('./libs/util')
var config = require('./config')
var reply = require('./wx/reply')
var Wechat = require('./wechat/wechat')


var app = new Koa()

var ejs = require('ejs')
var heredoc = require('heredoc')
var crypto = require('crypto')

var tpl = heredoc(function() {/*
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="UTF-8">
		<title>搜电影</title>
		<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale = 2.0">
		<script src="http://zeptojs.com/zepto-docs.min.js"></script>
		<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
		<script>
			wx.config({
			    debug: false, 
			    appId: 'wx920394da26edc0c4', // 必填，公众号的唯一标识
			    timestamp: '<%= timestamp %>', // 必填，生成签名的时间戳
			    nonceStr: '<%= noncestr %>', // 必填，生成签名的随机串
			    signature: '<%= signature %>',// 必填，签名，见附录1
			    jsApiList: [
			    	'onMenuShareTimeline',
					'onMenuShareAppMessage',
					'onMenuShareQQ',
					'onMenuShareWeibo',
					'onMenuShareQZone',
			    	'startRecord',
			    	'previewImage',
					'stopRecord',
					'onVoiceRecordEnd',
					'translateVoice'
			    ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2

			})

			wx.ready(function(){
				// 检查接口权限
			    wx.checkJsApi({
				    jsApiList: ['onVoiceRecordEnd'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
				    success: function(res) {
				        console.log(res)
				    }
				})

			    var shareContent = {
			    	title: '默认分享', 
				    desc: '描述信息',
				    link: 'https://www.github.com', 
				    imgUrl: 'https://img.mukewang.com/5a28a6f2000161b309360316.jpg', 
				    success: function () { 
				    	window.alert('分享成功！')
				    },
				    cancel: function () { 
				    	window.alert('分享失败！')
			    	}
				}
			    wx.onMenuShareAppMessage(shareContent)

				var slides
				$('#poster').on('tap', function() {
					wx.previewImage(slides);
				})

			    // 点击标题开始录音
			    var isRecording = false
			    $('h1').on('tap', function() {
			    	if(!isRecording){
			    		isRecording = true
				    	wx.startRecord({
				    		cancel: function() {
				    			window.alert('您取消了搜索！')
				    		}
				    	});
				    	return
			    	}
			    	isRecording = false
			    	wx.stopRecord({
					    success: function (res) {
					        var localId = res.localId;
					        wx.translateVoice({
							    localId: localId, // 需要识别的音频的本地Id，由录音相关接口获得
							    isShowProgressTips: 1, // 默认为1，显示进度提示
							    success: function (res) {
							        // window.alert(res.translateResult); // 语音识别的结果
							        var result = res.translateResult
							        $.ajax({
							        	type: 'get',
							        	url: 'https://api.douban.com/v2/movie/search?q=' + result,
							        	dataType: 'jsonp',
							        	jsonp: 'callback',
							        	success: function(data){
							        		var subject = data.subjects[0]
							        		$(title).html(subject.title)
							        		$(year).html(subject.year)
							        		$(director).html(subject.directors[0].name)
							        		$(poster).html('<image src="' + subject.images.large + '" />')
							        		shareContent = {
											    title: subject.title, 
											    desc: '我搜出来了：' + subject.title, 
											    link: 'https://www.github.com', 
											    imgUrl: subject.images.large, 
											    success: function () { 
											    	window.alert('分享成功！')
											    },
											    cancel: function () { 
											    	window.alert('分享失败！')
											    }
											}
											wx.onMenuShareAppMessage(shareContent)

											slides = {
												current: subject.images.large,
												urls: [subject.images.large]
											}
											data.subjects.forEach(function(item) {
												slides.urls.push(item.images.large)
											})
							        	}
							        })
							    }
							});
					    }
					})
			    })
			})
		</script>
	</head>
	<body>
		<h1>点击标题开始录音翻译</h1>
		<p id="title"></p>
		<div id="director"></div>
		<div id="year"></div>
		<div id="poster"></div>
	</body>
	</html>
*/})

var createNonce = function(){
	return Math.random().toString(36).substr(2, 15)
}

var createTimestamp = function(){
	return parseInt(new Date().getTime()/1000, 10) + ''
}

var _sign = function(noncestr, ticket, timestamp, url){
	var params = [
		'noncestr=' + noncestr,
		'jsapi_ticket=' + ticket,
		'timestamp=' + timestamp,
		'url=' + url
	]
	var str = params.sort().join('&')
	var shasum = crypto.createHash('sha1')
	shasum.update(str)
	return shasum.digest('hex')
}

function sign(ticket, url){
	var noncestr = createNonce()
	var timestamp = createTimestamp()
	var signature = _sign(noncestr, ticket, timestamp, url)
	return {
		noncestr: noncestr,
		timestamp: timestamp,
		signature: signature
	}
}

app.use(convert(function *(next) {
	if(this.url.indexOf('/movie') > -1){
		var wechatApi = new Wechat(config.weChat)
		var data = yield wechatApi.fetchAccessToken()
		var access_token = data.access_token
		var ticketData = yield wechatApi.fetchTicket(access_token)
		var ticket = ticketData.ticket
		var url = this.href
		var params = sign(ticket, url)
		console.log(params)
		this.body = ejs.render(tpl, params)
		return next
	}
	yield next
}))

// 将config对象自变量传递给中间件g.js
app.use(wechat(config.weChat, reply.reply))

app.listen(1234)

console.log("listening 1234")
