'use strict'

// 中间件
var sha1 = require('sha1')
var getRawBody = require('raw-body')
var convert = require('koa-convert');
var Wechat = require('./wechat')
var util = require('./util')

// 获取app.js传递进来的参数
module.exports = function(opts, handler){
	// 声明一个Wechat对象
	var wechat = new Wechat(opts)
	return convert(function *(next){
		// var that = this
		var token = opts.token
		var signature = this.query.signature
		var timestamp = this.query.timestamp
		var nonce = this.query.nonce
		var echostr = this.query.echostr
		var str = [token,timestamp,nonce].sort().join('')
		var sha = sha1(str)
		if(this.method == 'GET'){
			if(sha === signature){
				this.body = echostr + ''
			}else{
				this.body = 'wrong'
			}
		}
		else if(this.method == 'POST'){

			if(sha !== signature){
				this.body = 'wrong'
				return false
			}

			var data = yield getRawBody(this.req, {
				length: this.length,
				limit: '1mb',
				encoding: this.charset
			})

			var content = yield util.parseXMLAsync(data)
			var message = util.formatMessage(content.xml)

			this.weixin = message
			
			yield handler.call(this, next)

			wechat.reply.call(this)
		}
	})
}

