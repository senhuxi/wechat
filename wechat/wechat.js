'use strict'

var Promise = require('bluebird')
var request = Promise.promisify(require('request'))
var util = require('./util')
var fs = require('fs')
var _ = require('lodash')

var prefix = "https://api.weixin.qq.com/cgi-bin/"// 微信接口主地址
var api = {
	accessToken: prefix + 'token?grant_type=client_credential',// 获取accessToken
	// 上传临时素材
	temporary: {
		upload: prefix + 'media/upload?',// 上传
		fetch: prefix + 'media/get?'// 获取
	},
	// 上传永久素材
	permanent: {
		upload: prefix + 'material/add_material?',// 上传其他类型素材
		uploadNews: prefix + 'material/add_news?',// 上传图文素材
		uploadNewsPic: prefix + 'media/uploadimg?',// 上传图片素材
		fetch: prefix + 'material/get_material?',// 获取素材
		del: prefix + 'material/del_material?',// 删除素材
		update: prefix + 'material/update_news?',// 更新素材
		count: prefix + 'get_materialcount?',// 素材总数
		batch: prefix + 'batchget_material?'// 素材列表
	},
	// 标签
	tags: {
		create: prefix + 'tags/create?',// 创建标签
		fetch: prefix + 'tags/get?',// 获取标签
		update: prefix + 'tags/update?',// 更新标签
		del: prefix + 'tags/delete?',// 删除标签
		fetchUsers: prefix + 'user/tag/get?',// 获取标签下用户信息
		members: prefix + 'tags/members?',// 为用户批量打标签
		batchuntagging: prefix + 'tags/members/batchuntagging?',// 批量为用户取消标签
		getidlist: prefix + 'tags/getidlist?'// 获取用户身上的标签列表
	},
	// 用户
	user: {
		remark: prefix + "user/info/updateremark?",// 更新用户昵称
		fetch: prefix + "user/info?",// 获取用户信息
		batch: prefix + "user/info/batchget?",// 批量获取用户信息
		userlist: prefix + "user/get?"// 获取用户列表
	},
	// 群发消息
	mass: {
		tag: prefix + "message/mass/sendall?",// 标签下用户消息群发
		masssend: prefix + "message/mass/send?",// 所有用户消息群发
		del: prefix + "message/mass/delete?",// 删除群发消息
		preview: prefix + "message/mass/preview?",// 预览群发消息
		check: prefix + "message/mass/get?"// 获取消息发送状态
	},
	// 菜单
	menu: {
		create: prefix + "menu/create?",// 创建菜单
		fetch: prefix + "menu/get?",// 查询菜单
		del: prefix + "menu/delete?",// 删除菜单
		current: prefix + "get_current_selfmenu_info?"// 获取自定义菜单配置
	},
	// sdk票据（api_ticket）
	ticket: {
		fetch: prefix + "ticket/getticket?"
	}
}

// 构造函数
function Wechat(opts) {
	// 获取app.js传递进来的属性、方法
	var that = this
	this.appID = opts.appID
	this.appSecret = opts.appSecret
	this.getAccessToken = opts.getAccessToken
	this.saveAccessToken = opts.saveAccessToken
	this.getTicket = opts.getTicket
	this.saveTicket = opts.saveTicket
	
	this.fetchAccessToken()
}

// 验证token的合法性
Wechat.prototype.isValidAccessToken = function(data) {

	if(!data || !data.access_token || !data.expires_in){
		return false
	}

	var access_token = data.access_token
	var expires_in = data.expires_in
	var now = (new Date().getTime())

	// 验证token是否过期
	if(now < expires_in){
		return true
	}else{
		return false
	}
}

// 更新token
Wechat.prototype.updateAccessToken = function() {

	var appID = this.appID
	var appSecret = this.appSecret
	var url = api.accessToken + '&appid=' + appID + "&secret=" + appSecret

	return new Promise(function(resolve, reject) {
		request({
			url: url,
			json: true
		}).then(function(response){
			var data = response.body
			var now = (new Date().getTime())
			var expires_in = now + (data.expires_in - 20) * 1000
			data.expires_in = expires_in
			resolve(data)
		})
	})
}

// 获取AccessToken
Wechat.prototype.fetchAccessToken = function(data){

	var that = this
	// if(this.access_token && this.expires_in){
	// 	if(this.isValidAccessToken(this)) {
	// 		return Promise.resolve(this)
	// 	}
	// }

	return this.getAccessToken()
	// 检查token的合法性
	.then(function(data) {
		try{
			data = JSON.parse(data)
			// token合法，则向下传递
			if (that.isValidAccessToken(data)){
				return Promise.resolve(data)
			}else{
				// 不合法更新token
				return that.updateAccessToken()
			}
		}
		catch (e){
			return that.updateAccessToken()
		}
		
	})
	// token检查完成之后，将新的token值和过期时间写入data,同时保存token
	.then(function(data) {
		that.access_token = data.access_token
		that.expires_in = data.expires_in
		that.saveAccessToken(data)
		return Promise.resolve(data)
	})
}

// 验证Ticket
Wechat.prototype.isValidTicket = function(data) {

	if(!data || !data.ticket || !data.expires_in){
		return false
	}

	var ticket = data.ticket
	var expires_in = data.expires_in
	var now = (new Date().getTime())

	// 验证token是否过期
	if(ticket && now < expires_in){
		return true
	}else{
		return false
	}
}

// 更新Ticket
Wechat.prototype.updateTicket = function(access_token) {

	var url = api.ticket.fetch + '&access_token=' + access_token + '&type=jsapi'

	return new Promise(function(resolve, reject) {
		request({
			url: url,
			json: true
		}).then(function(response){
			var data = response.body
			var now = (new Date().getTime())
			var expires_in = now + (data.expires_in - 20) * 1000
			data.expires_in = expires_in
			resolve(data)
		})
	})
}

// 获取apiticket
Wechat.prototype.fetchTicket = function(access_token){

	var that = this

	return this.getTicket()
	// 检查Ticket的合法性
	.then(function(data) {
		try{
			data = JSON.parse(data)
			// Ticket合法，则向下传递
			if (that.isValidTicket(data)){
				return Promise.resolve(data)
			}else{
				// 不合法更新Ticket
				return that.updateTicket(access_token)
			}
		}
		catch (e){
			return that.updateTicket(access_token)
		}
		
	})
	// token检查完成之后，将新的token值和过期时间写入data,同时保存token
	.then(function(data) {
		that.saveTicket(data)
		return Promise.resolve(data)
	})
}

// 增加上传方法
// permanent是否永久素材
Wechat.prototype.uploadMeterial = function(type, material, permanent) {
	var that = this
	var form = {}
	var uplaodUrl = api.temporary.upload// 默认临时素材上传
	// 永久素材处理
	if(permanent){
		uplaodUrl = api.permanent.upload
		_.extend(form, permanent)// form继承permanent自变量
	}

	if(type === 'pic'){// 图片
		uplaodUrl = api.permanent.uplaodNewsPic
	}
	if(type === 'news'){// 图文
		uplaodUrl = api.permanent.uplaodNews
		form = material
	}
	else{// 其他
		form.media = fs.createReadStream(material)
	}
	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = uplaodUrl + 'access_token=' + data.access_token
			if(!permanent){
				url += "&type=" + type
			}else{
				form.access_token = data.access_token
			}

			var options = {
				method: 'POST',
				url: url,
				json: true
			}

			if(type === 'news'){
				options.body = form
			}
			else{
				options.formData = form
				url += "&type=" + type
			}

			request(options).then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('uploadMeterial fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 获取素材
Wechat.prototype.fetchMeterial = function(mediaId, type, permanent) {

	var that = this
	var form = {}

	var fetchUrl = api.temporary.fetch
	if(permanent){
		var fetchUrl = api.permanent.fetch
	}

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = fetchUrl + 'access_token=' + data.access_token

			var options = {
				method: 'POST',
				url: url,
				body: form,
				json: true
			}

			if(permanent){
				form.media_id = mediaId,
				form.access_token = data.access_token,
				options.body = from
			}
			else{
				if(type === 'video'){
					url = url.replace('https://', 'http://')// 视频信息修改协议
				}
				url += '&media_id=' + mediaId
			}

			// 若获取图文和视频直接返回素材信息，其他返回调用url
			if(type === 'news' || type === 'video'){
				request(options)
				.then(function(response){
					var _data = response.body
					if(_data){
						resolve(_data)
					}
					else{
						throw new Error('fetchMeterial fails')
					}
				})
				.catch(function(err){
					reject(err)
				})
			}else{
				resolve(url)
			}
		})
	})
}

// 删除永久素材
Wechat.prototype.deleteMeterial = function(mediaId) {

	var that = this
	var form = {
		media_id: mediaID
	}

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.permanent.del + 'access_token=' + data.access_token
			request({
				method: 'POST',
				url: url,
				body: form,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('deleteMeterial fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 更新永久素材
Wechat.prototype.updateMeterial = function(mediaId, news) {

	var that = this
	var form = {
		media_id: mediaID
	}

	_.extend(form, news)

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.permanent.updateUrl + 'access_token=' + data.access_token
			
			request({
				method: 'POST',
				url: url,
				body: form,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('updateMeterial fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 获取素材总数
Wechat.prototype.countMeterial = function() {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.permanent.count + 'access_token=' + data.access_token
			
			request({
				method: 'GET',
				url: url,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('countMeterial fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 获取素材列表
Wechat.prototype.batchMeterial = function(options) {

	var that = this
	options.type = options.type || 'image'// 默认获取图片素材
	options.offset = options.offset || 0// 默认从0开始
	options.count = options.count || 20// 默认获取20个

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.permanent.batch + 'access_token=' + data.access_token
			
			request({
				method: 'POST',
				url: url,
				body: options,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('batchMeterial fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 创建标签
Wechat.prototype.createTag = function(name) {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.tags.create + 'access_token=' + data.access_token
			
			var form = {
				tag : {
			    	name : name//标签名
			  	}
			 }

			request({
				method: 'POST',
				url: url,
				body: form,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('createTag fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 设置备注名
Wechat.prototype.remarkUser = function(openId, remark) {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.user.remark + 'access_token=' + data.access_token
			
			var form = {
				opendi: openId,
				remark: remark
			 }

			request({
				method: 'POST',
				url: url,
				body: form,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('remarkUser fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 获取用户信息（单个、多个）
Wechat.prototype.fetchUsers = function(openIds, lang) {

	var that = this
	lang = lang || "zh_CN"// 默认汉语

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var options = {
				json: true
			}
			var url
			if(_.isArray(openIds)){// 多用户
				url = api.user.batch + 'access_token=' + data.access_token
				options.body = {
					user_list: openIds
				}
				options.method = 'POST'
				options.url = url
			}
			else{// 单用户
				url = api.user.fetch + 'access_token=' + data.access_token
				url += '&openid=' + openIds + '&lang=' + lang
				options.method = 'GET'
				options.url = url
			}

			request(options)
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('fetchUsers fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 获取用户列表
Wechat.prototype.fetchUserList = function(nextOpenid) {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.user.userlist + 'access_token=' + data.access_token
			if(nextOpenid){
				url += '&next_openid=' + nextOpenid
			}
			request({
				method: 'GET',
				url: url,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('fetchUserList fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 用户消息群发
Wechat.prototype.sendByTag = function(type, message, tagId) {

	var that = this
	var msg = {
		filter: {},
		msgType: type
	}
	msg[type] = message

	if(!tagId){// 所有用户消息群发
		msg.filter.is_to_all = true
	}
	else{// 标签内用户消息群发
		msg.filter = {
	      is_to_all: false,
	      tag_id: tagId
   		}
	}

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.mass.tag + 'access_token=' + data.access_token
			if(nextOpenid){
				url += '&next_openid=' + nextOpenid
			}
			request({
				method: 'POST',
				url: url,
				body: msg,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('sendByTag fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 服务号发送消息
Wechat.prototype.sendByOpenId = function(type, message, openIds) {

	var that = this
	var msg = {
		msgType: type,
		touser: openIds
	}
	msg[type] = message

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.mass.masssend + 'access_token=' + data.access_token
			if(nextOpenid){
				url += '&next_openid=' + nextOpenid
			}
			request({
				method: 'POST',
				url: url,
				body: msg,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('sendByOpenId fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 删除群发（半小时内）
Wechat.prototype.deleteMass = function(messageId) {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.mass.del + 'access_token=' + data.access_token
			var form = {
				msg_id: messageId
			}
			if(nextOpenid){
				url += '&next_openid=' + nextOpenid
			}
			request({
				method: 'POST',
				url: url,
				body: form,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('deleteMass fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 预览消息
Wechat.prototype.previewMsg = function(type, message, openId) {

	var that = this
	var msg = {
		msgType: type,
		touser: openId
	}
	msg[type] = message

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.mass.preview + 'access_token=' + data.access_token
			if(nextOpenid){
				url += '&next_openid=' + nextOpenid
			}
			request({
				method: 'POST',
				url: url,
				body: msg,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('previewMsg fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 检查消息是否送达
Wechat.prototype.checkMass = function(messageId) {

	var that = this
	var form = {
		msg_id: messageId
	}

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.mass.check + 'access_token=' + data.access_token
			if(nextOpenid){
				url += '&next_openid=' + nextOpenid
			}
			request({
				method: 'POST',
				url: url,
				body: form,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('previewMsg fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 创建菜单
Wechat.prototype.createMenu = function(menu) {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.menu.create + 'access_token=' + data.access_token
			request({
				method: 'POST',
				url: url,
				body: menu,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('createMenu fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 获取菜单
Wechat.prototype.getMenu = function() {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.menu.fetch + 'access_token=' + data.access_token
			request({
				method: 'GET',
				url: url,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('getMenu fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 删除菜单
Wechat.prototype.deleteMenu = function() {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.menu.del + 'access_token=' + data.access_token
			request({
				method: 'GET',
				url: url,
				json: true
			})
			.then(function(response){
				var _data = response.body
				console.log(JSON.stringify(_data))
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('deleteMenu fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 获取自定义配置菜单
Wechat.prototype.getCurrentMenu = function() {

	var that = this

	return new Promise(function(resolve, reject) {
		that
		.fetchAccessToken()
		.then(function(data) {
			var url = api.menu.current + 'access_token=' + data.access_token
			request({
				method: 'GET',
				url: url,
				json: true
			})
			.then(function(response){
				var _data = response.body
				if(_data){
					resolve(_data)
				}
				else{
					throw new Error('getCurrentMenu fails')
				}
			})
			.catch(function(err){
				reject(err)
			})
		})
	})
}

// 用户信息回复
Wechat.prototype.reply = function() {
	var content = this.body
	var message = this.weixin

	var xml = util.tpl(content, message)
	console.log("return xml:"+xml)
	
	this.status = 200
	this.type = 'application/xml'
	this.body = xml
}

module.exports = Wechat