'use strict'// 严格模式
var convert = require('koa-convert');
var path = require('path')
var Promise = require('bluebird')
var config = require('../config')
var menu = require('./menu')
var Wechact = require('../wechat/wechat')
var wechatApi = new Wechact(config.weChat)

// wechatApi.deleteMenu().then(function() {
// 	return wechatApi.createMenu(menu)
// })
// .then(function(msg) {
// 	console.log(msg)
// })

exports.reply = function *(next) {
	var message = this.weixin
	if(message.MsgType === 'event'){// 事件推送
		if(message.Event === 'subscribe'){
			if(message.EventKey){
				console.log('扫描二维码进来：' + message.EventKey + ' ' + message.ticket)
			}

			this.body = '您订阅了这个微信公众号'
		} 
		else if(message.Event === 'unsubscribe'){
			console.log('无情取关')
			this.body = ''
		}
		else if(message.Event === 'LOCATION'){
			this.body = '您上报的位置是：' + message.Latitude + '/' + message.Longitude+ '-' +  message.Precision
		}
		else if(message.Event === 'CLICK'){
			this.body = '您点击了菜单：' + message.EventKey
		}
		else if(message.Event === 'SCAN'){
			console.log('关注后扫二维码：' + message.EventKey + ' ' + message.Ticket)
			this.body = '看到你扫了一下'
		}
		else if(message.Event === 'VIEW'){
			this.body = '您点击了菜单中的链接：' + message.EventKey
		}
		else if(message.Event === 'scancode_push'){
			console.log(message.ScanCodeInfo.ScanType)
			console.log(message.ScanCodeInfo.ScanResult)
			this.body = '您点击了菜单中：' + message.EventKey
		}
		else if(message.Event === 'scancode_waitmsg'){
			console.log(message.ScanCodeInfo.ScanType)
			console.log(message.ScanCodeInfo.ScanResult)
			this.body = '您点击了菜单中：' + message.EventKey
		}
		else if(message.Event === 'pic_sysphoto'){
			console.log(message.SendPicsInfo.PicList)
			console.log(message.SendPicsInfo.Count)
			this.body = '您点击了菜单中：' + message.EventKey
		}
		else if(message.Event === 'pic_photo_or_album'){
			console.log(message.SendPicsInfo.PicList)
			console.log(message.SendPicsInfo.Count)
			this.body = '您点击了菜单中：' + message.EventKey
		}
		else if(message.Event === 'pic_weixin'){
			console.log(message.SendPicsInfo.PicList)
			console.log(message.SendPicsInfo.Count)
			this.body = '您点击了菜单中：' + message.EventKey
		}
		else if(message.Event === 'location_select'){
			console.log(message.SendLocationInfo.Location_X)
			console.log(message.SendLocationInfo.Location_Y)
			console.log(message.SendLocationInfo.Scale)
			console.log(message.SendLocationInfo.Label)
			console.log(message.SendLocationInfo.Poiname)
			this.body = '您点击了菜单中：' + message.EventKey
		}
	}
	else if (message.MsgType === 'text'){
		var content = message.Content
		var reply = '额，你说的' + content + '太复杂了'

		if(content === '1'){
			var reply = '天下第一吃大米'
		}
		else if(content === '2'){
			var reply = '天下第二吃豆腐'
		}
		else if(content === '3'){
			var reply = '天下第三吃仙丹'
		}
		else if(content === '4'){
			var reply = [{
				title: '技术改变世界',
				description: '只是个描述',
				picUrl: 'http://img.taopic.com/uploads/allimg/130712/240387-130G20Q13295.jpg',
				url: 'https://nodejs.org'
			}
			// ,{
			// 	title: 'nodeJs学习',
			// 	description: '真是不错',
			// 	picUrl: 'http://pic.58pic.com/58pic/13/74/45/34X58PICc3y_1024.jpg',
			// 	url: 'https://nodejs.org'
			// }
			]
		}
		else if(content === '5'){
			var data = yield wechatApi.uploadMeterial('image', path.join(__dirname, '../2.jpg'))
			reply = {
				type: 'image',
				mediaId: data.media_id
			}
		}
		else if(content === '6'){
			var data = yield wechatApi.uploadMeterial('video', path.join(__dirname, '../3.mp4'))
			reply = {
				type: 'video',
				title: '回复视频',
				description: '描述信息',
				mediaId: data.media_id
			}
		}
		else if(content === '7'){
			var data = yield wechatApi.uploadMeterial('image', path.join(__dirname, '../2.jpg'))
			reply = {
				type: 'music',
				title: '回复音乐内容',
				description: '描述音乐',
				musicUrl: 'http://ws.stream.qqmusic.qq.com/C100003507bR0gDKBm.m4a?fromtag=38',
				thumbMediaId: data.media_id
			}
		}
		else if(content === '8'){
			var data = yield wechatApi.uploadMeterial('image', path.join(__dirname, '../2.jpg'), {type: 'image'})
			reply = {
				type: 'image',
				mediaId: data.media_id
			}
		}
		else if(content === '9'){
			var data = yield wechatApi.uploadMeterial('video', path.join(__dirname, '../3.mp4'), {type: 'video', description:'{"title": "nice", "introduction": "nerver"}'})
			reply = {
				type: 'music',
				title: '回复音乐内容',
				description: '描述音乐',
				musicUrl: 'http://ws.stream.qqmusic.qq.com/C100003507bR0gDKBm.m4a?fromtag=38',
				thumbMediaId: data.media_id
			}
		}

		this.body = reply
		
	}

	yield next
}